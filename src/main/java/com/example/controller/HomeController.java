package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.bean.Persona;
import com.example.service.PersonaService;

@Controller
@RequestMapping("/app")
public class HomeController {

	@Autowired
	private PersonaService personaService;
	 
	@GetMapping
	public String index() {
		return "index";
	}
	
	@GetMapping("/personas")
	public String listaPersonas(Model model) { 
		model.addAttribute("personas", personaService.getPersonas());
		return "lista";
	}
	
	@GetMapping("/personas/form")
	public String personaForm(Model model) {
		model.addAttribute("persona", new Persona());
		return "form";
	}
	

	@PostMapping("/personas")
	public String agregarPersonas(@ModelAttribute("persona") Persona persona) {
		
		personaService.agregarPersona(persona);
		return "redirect:/app/personas";
	}
	
}
